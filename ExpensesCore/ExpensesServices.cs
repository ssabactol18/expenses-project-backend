﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExpensesDB;

namespace ExpensesCore
{
    public class ExpensesServices : IExpensesServices
    {
        private ExpenseContext _context;
        
        public ExpensesServices(ExpenseContext context)
        {
            _context = context;
        }
        public List<Expense> GetExpenses()
        {
            return _context.Expenses.ToList();
        }

        public Expense GetExpense(int id)
        {
            return _context.Expenses.First(expense => expense.Id == id);
        }

        public Expense CreateExpense(Expense expense)
        {

            _context.Add(expense);
            _context.SaveChanges();

            return expense;

        }

        public void DeleteExpense(Expense expense)
        {
            _context.Expenses.Remove(expense);
            _context.SaveChanges();
        }

        public Expense UpdateExpense(Expense expense)
        {
            var UpdatedExpense = _context.Expenses.First(exp => exp.Id == expense.Id);
            UpdatedExpense.Description = expense.Description;
            UpdatedExpense.Amount = expense.Amount;
            _context.SaveChanges();

            return UpdatedExpense;
        }
    }
}