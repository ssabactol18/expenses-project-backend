﻿using System.Collections.Generic;
using ExpensesDB;

namespace ExpensesCore
{
    public interface IExpensesServices
    {
        List<Expense> GetExpenses();
        Expense GetExpense(int id);

        Expense CreateExpense(Expense expense);

        void DeleteExpense(Expense expense);

        Expense UpdateExpense(Expense expense);
    }
}