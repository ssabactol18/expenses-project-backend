﻿using Microsoft.EntityFrameworkCore;

namespace ExpensesDB
{
    public class ExpenseContext : DbContext
    {
        public DbSet<Expense> Expenses { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
                @"Server=(localdb)\mssqllocaldb;Database=Expense_DB;Integrated Security=True");
        }
    }
}